# 树莓派摄像头实时检测应用<a name="ZH-CN_TOPIC_0000001641068454"></a>

## 样例介绍

功能：使用yolov3目标检测模型对树莓摄像头中的即时视频进行目标检测。
样例输入：树莓摄像头视频。
样例输出：树莓派os中界面展现检测结果。

## 样例下载<a name="section41941924142412"></a>

可以使用以下两种方式下载，请选择其中一种进行源码准备。

-   命令行方式下载（**下载时间较长，但步骤简单**）。

    ```
    # 开发环境，非root用户命令行中执行以下命令下载源码仓。    
    cd ${HOME}     
    git clone https://gitee.com/HiCAID/rpi-ascend-app.git
    ```

-   压缩包方式下载（**下载时间较短，但步骤稍微复杂**）。

    ```
    # 1. samples仓右上角选择 【克隆/下载】 下拉框并选择 【下载ZIP】。     
    # 2. 将ZIP包上传到开发环境中的普通用户家目录中，【例如：${HOME}/rpi-ascend-app-master.zip】。      
    # 3. 开发环境中，执行以下命令，解压zip包。      
    cd ${HOME}     
    unzip rpi-ascend-app-master.zip
    ```


**样例的代码目录说明如下：**

```
├── scripts       // 用于存放运行样例的脚本                                              
├── src          // 用于存放源码
```

## 准备环境<a name="section1835415517713"></a>

1.  安装CANN软件。

    单击[Link](https://hiascend.com/document/redirect/CannCommunityInstSoftware)，获取最新版本的CANN软件安装指南。

    **注意：**此处还可以在页面左上侧切换版本，查看对应版本的安装指南。

2.  设置环境变量。

    **注：**“$HOME/Ascend”请替换“Ascend-cann-toolkit”包的实际安装路径。

    ```
    # 设置CANN依赖的基础环境变量
    . ${HOME}/Ascend/ascend-toolkit/set_env.sh
    
    #如果用户环境存在多个python3版本，则指定使用python3.11.2版本
    export PATH=/usr/local/python3.11.2/bin:$PATH
    #设置python3.11.2库文件路径
    export LD_LIBRARY_PATH=/usr/local/python3.11.2/lib:$LD_LIBRARY_PATH
    
    # 配置程序编译依赖的头文件与库文件路径
    export DDK_PATH=$HOME/Ascend/ascend-toolkit/latest 
    export NPU_HOST_LIB=$DDK_PATH/runtime/lib64/stub
    ```

3.  安装picamera2、OpenCV。

    执行以下命令安装opencv

    ```
    sudo apt install -y python3-libcamera
    sudo apt install -y python3-picamera2
    sudo apt-get install libopencv-dev
    ```

## 样例运行<a name="section012033382418"></a>

1.  **获取是基于Caffe的yolov3目标检测模型，并转换为昇腾AI处理器能识别的模型（\*.om）。**

    **注：**此处以昇腾310 AI处理器为例，针对其它昇腾AI处理器的模型转换，需修改atc命令中的--soc\_version参数值。

    ```
    # 为了方便下载，在这里直接给出原始模型下载及模型转换命令,可以直接拷贝执行。      
    cd $HOME/rpi-ascend-app/asset/model  
    wget https://obs-9be7.obs.cn-east-2.myhuaweicloud.com/003_Atc_Models/AE/ATC%20Model/Yolov3/aipp_nv12.cfg    
    wget https://obs-9be7.obs.cn-east-2.myhuaweicloud.com/003_Atc_Models/AE/ATC%20Model/Yolov3/yolov3.caffemodel
    wget https://obs-9be7.obs.cn-east-2.myhuaweicloud.com/003_Atc_Models/AE/ATC%20Model/Yolov3/yolov3.prototxt
    atc --model=yolov3.prototxt --weight=yolov3.caffemodel --framework=0 --output=object_detection --soc_version=Ascend310B1 --insert_op_conf=aipp_nv12.cfg     
    ```

    atc命令中各参数的解释如下，详细约束说明请参见[《ATC模型转换指南》](https://hiascend.com/document/redirect/CannCommunityAtc)。
    
    >**说明：** 
    >如果无法确定当前设备的soc\_version，则在安装驱动包的服务器执行**npu-smi info**命令进行查询，在查询到的“Name“前增加Ascend信息，例如“Name“对应取值为_xxxyy_，实际配置的soc\_version值为Ascend_xxxyy_。
    

为了方便样例运行提供了对应的数据在$HOME/rpi-ascend-app/asset/model下，样例中的路径也是此模型，不想体验模型转换的也可以直接运行。


2. **运行样例。**

   执行以下脚本运行样例：

   ```
   cd $HOME/rpi-ascend-app/app/Yolov3_Raspi_Camera/python/scripts
   bash sample_run.sh
   ```

   执行成功后，在屏幕上会实时展示检测结果

   ![输入图片说明](../../../asset/readme_pic/CameraPic.jpg)


## 相关操作<a name="section27901216172515"></a>

- 获取学习文档，请单击[AscendCL C&C++](https://hiascend.com/document/redirect/CannCommunityCppAclQuick)或[AscendCL Python](https://hiascend.com/document/redirect/CannCommunityPyaclQuick)，查看最新版本的AscendCL推理应用开发指南。
- 查模型的输入输出

  可使用第三方工具Netron打开网络模型，查看模型输入或输出的数据类型、Shape，便于在分析应用开发场景时使用。
