import os
import cv2
import numpy as np
import acl
from picamera2 import Picamera2
import time

NPY_FLOAT32 = 11
ACL_MEMCPY_HOST_TO_HOST = 0
ACL_MEMCPY_HOST_TO_DEVICE = 1
ACL_MEMCPY_DEVICE_TO_HOST = 2
ACL_MEMCPY_DEVICE_TO_DEVICE = 3
ACL_MEM_MALLOC_HUGE_FIRST = 0
ACL_DEVICE, ACL_HOST = 0, 1
ACL_SUCCESS = 0
labels = ["person",
        "bicycle", "car", "motorbike", "aeroplane",
        "bus", "train", "truck", "boat", "traffic light",
        "fire hydrant", "stop sign", "parking meter", "bench",
        "bird", "cat", "dog", "horse", "sheep", "cow", "elephant",
        "bear", "zebra", "giraffe", "backpack", "umbrella", "handbag",
        "tie", "suitcase", "frisbee", "skis", "snowboard", "sports ball",
        "kite", "baseball bat", "baseball glove", "skateboard", "surfboard",
        "tennis racket", "bottle", "wine glass", "cup", "fork", "knife", "spoon",
        "bowl", "banana", "apple", "sandwich", "orange", "broccoli", "carrot", "hot dog",
        "pizza", "donut", "cake", "chair", "sofa", "potted plant", "bed", "dining table",
        "toilet", "TV monitor", "laptop", "mouse", "remote", "keyboard", "cell phone",
        "microwave", "oven", "toaster", "sink", "refrigerator", "book", "clock", "vase",
        "scissors", "teddy bear", "hair drier", "toothbrush"]

class Yolov3_Raspi_Camera(object):
    def __init__(self, device_id, model_path, model_width, model_height):
        self.device_id = device_id      # int
        self.context = None             # pointer
        self.stream = None

        self.model_width = model_width
        self.model_height = model_height
        self.model_id = None            # pointer
        self.model_path = model_path    # string
        self.model_desc = None          # pointer when using
        self.input_dataset = None
        self.output_dataset = None
        self.input_buffer0 = None
        self.input_buffer1 = None
        self.output_buffer0 = None
        self.output_buffer1 = None
        self.input_buffer_size0 = None
        self.input_buffer_size1 = None
        self.image_bytes = None
        self.image_name = None
        self.dir = None
        self.image = None
        self.runMode_ = acl.rt.get_run_mode()

    def init_resource(self):
        # init acl resource
        ret = acl.init()
        if ret != ACL_SUCCESS:
            print('acl init failed, errorCode is', ret)
        ret = acl.rt.set_device(self.device_id)
        if ret != ACL_SUCCESS:
            print('set device failed, errorCode is', ret)
        self.context, ret = acl.rt.create_context(self.device_id)
        if ret != ACL_SUCCESS:
            print('create context failed, errorCode is', ret)
        self.stream, ret = acl.rt.create_stream()
        if ret != ACL_SUCCESS:
            print('create stream failed, errorCode is', ret)
        # load model from file
        self.model_id, ret = acl.mdl.load_from_file(self.model_path)
        if ret != ACL_SUCCESS:
            print('load model failed, errorCode is', ret)
        # create description of model
        self.model_desc = acl.mdl.create_desc()
        ret = acl.mdl.get_desc(self.model_desc, self.model_id)
        if ret != ACL_SUCCESS:
            print('get desc failed, errorCode is', ret)
        # create data set of input
        self.input_dataset = acl.mdl.create_dataset()
        input_index0 = 0
        input_index1 = 1
        self.input_buffer_size0 = acl.mdl.get_input_size_by_index(self.model_desc, input_index0)
        self.input_buffer0, ret = acl.rt.malloc(self.input_buffer_size0, ACL_MEM_MALLOC_HUGE_FIRST)
        input_data0 = acl.create_data_buffer(self.input_buffer0, self.input_buffer_size0)
        self.input_buffer_size1 = acl.mdl.get_input_size_by_index(self.model_desc, input_index1)
        self.input_buffer1, ret = acl.rt.malloc(self.input_buffer_size1, ACL_MEM_MALLOC_HUGE_FIRST)
        input_data1 = acl.create_data_buffer(self.input_buffer1, self.input_buffer_size1)
        self.input_dataset, ret = acl.mdl.add_dataset_buffer(self.input_dataset, input_data0)
        self.input_dataset, ret = acl.mdl.add_dataset_buffer(self.input_dataset, input_data1)
        if ret != ACL_SUCCESS:
            print('acl.mdl.add_dataset_buffer failed, errorCode is', ret)
        # create data set of output
        self.output_dataset = acl.mdl.create_dataset()
        output_index0 = 0
        output_index1 = 1
        output_buffer_size0 = acl.mdl.get_output_size_by_index(self.model_desc, output_index0)
        output_buffer_size1 = acl.mdl.get_output_size_by_index(self.model_desc, output_index1)
        self.output_buffer0, ret = acl.rt.malloc(output_buffer_size0, ACL_MEM_MALLOC_HUGE_FIRST)
        self.output_buffer1, ret = acl.rt.malloc(output_buffer_size1, ACL_MEM_MALLOC_HUGE_FIRST)
        output_data0 = acl.create_data_buffer(self.output_buffer0, output_buffer_size0)
        output_data1 = acl.create_data_buffer(self.output_buffer1, output_buffer_size1)
        _, ret = acl.mdl.add_dataset_buffer(self.output_dataset, output_data0)
        _, ret = acl.mdl.add_dataset_buffer(self.output_dataset, output_data1)
        if ret != ACL_SUCCESS:
            print('acl.mdl.add_dataset_buffer failed, errorCode is', ret)

    def process_input(self, frame):
        image = frame
        self.image = image
        # zoom image to model_width * model_height
        scale = max(self.image.shape[0] / self.model_height, self.image.shape[1] / self.model_width)
        resize_image = cv2.resize(self.image, (round(self.image.shape[1] / scale), round(self.image.shape[0] / scale)))
        shape_scale = resize_image.shape
        resize_scale = np.zeros([416,416,3], dtype=np.uint8)
        resize_scale[0:shape_scale[0], 0:shape_scale[1]] = resize_image
        # switch channel BGR to YUV
        image_rgb = cv2.cvtColor(resize_scale, cv2.COLOR_BGR2YUV_I420)
        self.image_bytes = np.frombuffer(image_rgb.tobytes())
    def inference(self):
        image_info = np.array([self.model_width, self.model_height,
                              self.model_width, self.model_height],
                              dtype = np.float32)
        # pass image data to input data buffer
        if self.runMode_ == ACL_DEVICE:
            kind = ACL_MEMCPY_DEVICE_TO_DEVICE
        else:
            kind = ACL_MEMCPY_HOST_TO_DEVICE
        if "bytes_to_ptr" in dir(acl.util):
            bytes_data = self.image_bytes.tobytes()
            ptr = acl.util.bytes_to_ptr(bytes_data)
            info_data = image_info.tobytes()
            ptr_info = acl.util.bytes_to_ptr(info_data)
        else:
            ptr = acl.util.numpy_to_ptr(self.image_bytes)
            ptr_info = acl.util.numpy_to_ptr(image_info)
        ret = acl.rt.memcpy(self.input_buffer0,
                            self.input_buffer_size0,
                            ptr,
                            self.input_buffer_size0,
                            kind)
        ret = acl.rt.memcpy(self.input_buffer1,
                            self.input_buffer_size1,
                            ptr_info,
                            self.input_buffer_size1,
                            kind)
        if ret != ACL_SUCCESS:
            print('memcpy failed, errorCode is', ret)
        # inference
        ret = acl.mdl.execute(self.model_id,
                              self.input_dataset,
                              self.output_dataset)
        if ret != ACL_SUCCESS:
            print('execute failed, errorCode is', ret)

    def get_result(self,frame,FPS):
        # get result from output data set
        output_index0 = 0
        output_index1 = 1
        output_data_buffer0 = acl.mdl.get_dataset_buffer(self.output_dataset, output_index0)
        output_data_buffer1 = acl.mdl.get_dataset_buffer(self.output_dataset, output_index1)
        output_data_buffer_addr0 = acl.get_data_buffer_addr(output_data_buffer0)
        output_data_buffer_addr1 = acl.get_data_buffer_addr(output_data_buffer1)
        output_data_size0 = acl.get_data_buffer_size(output_data_buffer0)
        output_data_size1 = acl.get_data_buffer_size(output_data_buffer1)
        ptr0, ret = acl.rt.malloc_host(output_data_size0)
        ptr1, ret = acl.rt.malloc_host(output_data_size1)
        # copy device output data to host
        if self.runMode_ == ACL_DEVICE:
            kind = ACL_MEMCPY_DEVICE_TO_HOST
        else:
            kind = ACL_MEMCPY_HOST_TO_HOST
        ret = acl.rt.memcpy(ptr0,
                            output_data_size0,
                            output_data_buffer_addr0,
                            output_data_size0,
                            kind)
        ret = acl.rt.memcpy(ptr1,
                            output_data_size1,
                            output_data_buffer_addr1,
                            output_data_size1,
                            kind)
        if ret != ACL_SUCCESS:
            print('memcpy failed, errorCode is', ret)

        index0 = 0
        index1 = 1
        dims0, ret = acl.mdl.get_cur_output_dims(self.model_desc, index0)
        dims1, ret = acl.mdl.get_cur_output_dims(self.model_desc, index1)

        if ret != ACL_SUCCESS:
            print('get output dims failed, errorCode is', ret)
        out_dim0 = dims0['dims']
        out_dim1 = dims1['dims']

        if "ptr_to_bytes" in dir(acl.util):
            bytes_data0 = acl.util.ptr_to_bytes(ptr0, output_data_size0)
            bytes_data1 = acl.util.ptr_to_bytes(ptr1, output_data_size1)
            data0 = np.frombuffer(bytes_data0, dtype=np.float32).reshape(out_dim0)
            data1 = np.frombuffer(bytes_data1, dtype=np.int32).reshape(out_dim1)
        else:
            data0 = acl.util.ptr_to_numpy(ptr0, out_dim0, NPY_FLOAT32)
            data1 = acl.util.ptr_to_numpy(ptr1, out_dim1, NPY_FLOAT32)
        data = [data0, data1]
        frame_all = analyze_inference_output(data,frame,FPS)
        ret = acl.rt.free_host(ptr0)
        ret = acl.rt.free_host(ptr1)
        if ret != ACL_SUCCESS:
            print('free host failed, errorCode is', ret)
        return frame_all

    def release_resource(self):
        # release resource includes acl resource, data set and unload model
        acl.rt.free(self.input_buffer0)
        acl.rt.free(self.input_buffer1)
        acl.mdl.destroy_dataset(self.input_dataset)

        acl.rt.free(self.output_buffer0)
        acl.rt.free(self.output_buffer1)
        acl.mdl.destroy_dataset(self.output_dataset)
        ret = acl.mdl.unload(self.model_id)
        if ret != ACL_SUCCESS:
            print('unload model failed, errorCode is', ret)

        if self.model_desc:
            acl.mdl.destroy_desc(self.model_desc)
            self.model_desc = None

        if self.stream:
            ret = acl.rt.destroy_stream(self.stream)
            if ret != ACL_SUCCESS:
                print('destroy stream failed, errorCode is', ret)
            self.stream = None

        if self.context:
            ret = acl.rt.destroy_context(self.context)
            if ret != ACL_SUCCESS:
                print('destroy context failed, errorCode is', ret)
            self.context = None

        ret = acl.rt.reset_device(self.device_id)
        if ret != ACL_SUCCESS:
            print('reset device failed, errorCode is', ret)
            
        ret = acl.finalize()
        if ret != ACL_SUCCESS:
            print('finalize failed, errorCode is', ret)

def analyze_inference_output(infer_output, origin_img,FPS):
    """post"""
    box_num = int(infer_output[1][0, 0])
    box_num = infer_output[1][0, 0]
    box_info = infer_output[0].flatten()
    scalex = origin_img.shape[1] / model_width
    scaley = origin_img.shape[0] / model_height
    if scalex > scaley:
        scaley =  scalex
    for n in range(int(box_num)):
        ids = int(box_info[5 * int(box_num) + n])
        label = labels[ids]
        score = box_info[4 * int(box_num)+n]
        lt_x = int(box_info[0 * int(box_num)+n] * scaley)
        lt_y = int(box_info[1 * int(box_num)+n] * scaley)
        rb_x = int(box_info[2 * int(box_num) + n] * scaley)
        rb_y = int(box_info[3 * int(box_num) + n] * scaley)
        #Organize the confidence into a string
        conf = str(round(score * 100, 2)) + "%"
        if label == "person":
            color = (0, 0, 190)
        elif label == "bicycle" or label == "car" or label == "motorbike" or label == "bus":
            color = (0, 255, 255)
        else:
            color = (255, 200, 0)
        if (rb_x - lt_x) < origin_img.shape[0]/2:
            font_size = (rb_x - lt_x)/origin_img.shape[0] * 2
        elif (rb_x - lt_x) < origin_img.shape[0]/3:
            font_size = (rb_x - lt_x)/origin_img.shape[0] * 3
        else:
            font_size = (rb_x - lt_x)/origin_img.shape[0]
        cv2.putText(origin_img, f"FPS:{FPS}", (0, 35),
 			            cv2.FONT_HERSHEY_TRIPLEX, 1, (0, 0, 190), 1)
        cv2.putText(origin_img, f"{conf} {label}", (lt_x, lt_y-10),
 			            cv2.FONT_HERSHEY_TRIPLEX, font_size, color, 1)
        cv2.rectangle(origin_img, (lt_x, lt_y), (rb_x, rb_y), color, 3)
    cv2.imshow("yolov3",origin_img)
    cv2.waitKey(1)
def picamera_video():
    picam2 = Picamera2()
    picam2.configure(picam2.create_preview_configuration(main={"format": "RGB888","size": (800, 450)}))
    picam2.options["quality"] = 95
    picam2.preview_configuration.align()
    picam2.start()
    return picam2
def picamera_capture(picam2):
    frame = picam2.capture_array("main")
    return frame
if __name__ == '__main__':
    device = 0
    model_width = 416
    model_height = 416
    current_dir = os.path.dirname(os.path.abspath(__file__))
    model_path = os.path.join(current_dir, "../../../../asset/model/yolov3.om")
    net = Yolov3_Raspi_Camera(device, model_path, model_width, model_height)
    net.init_resource()
    picam2 = picamera_video()
    while True:
        t0 = time.time()
        frame = picamera_capture(picam2)
        if frame is None:
            print("Camera has no frame or finish")
            break
        net.process_input(frame)
        net.inference()
        t1 = time.time()
        FPS = 1 / (t1 -t0)
        frame = net.get_result(frame,FPS)
    picam2.close()
    cv2.destroyAllWindows()
    net.release_resource()

