# rpi-ascend-app

#### 介绍

基于树莓派开发板利用  **全爱科技 QA200A2推理卡** (基于Atlas 200I A2 加速模块)  进行视频推理的Demo

加速模块驱动开源地址如下：https://gitee.com/HiCAID/rpi-ascend-driver/


### 硬件

1，树莓派使用CM4 IO Board的PCIE X1接口，通过转接卡接到PCIE Socket以便额外供电。
![CM4IOboard连接推理卡](asset/readme_pic/WechatIMG54.jpeg)

2，RK3588开发板
   
 有PCIE X2 Socket，在供电满足的情况下，直接插卡。
   


#### 软件架构

1、需要按照树莓派官方文档搭进行制卡搭建好树莓派基础环境，建议选用树莓派OS：https://www.raspberrypi.com/documentation/computers/getting-started.html#install-using-imager

2、需要安装指定的适配树莓派os的昇腾驱动

3、需要按照对应指导安装cann软件及其依赖


#### 安装教程

1. 获取指定的适配树莓派os的昇腾驱动

   下载地址：

   在root用户下执行：Ascend_xxx.run --full --install-for-all 安装驱动

   安装成功后下电设备再重新上电，执行npu-smi info查询芯片信息，有如图所示的结果说明安装成功

2. 安装cann软件及其依赖

   在当前用户下执行以下命令安装依赖

   ```linux
   sudo apt-get update
   sudo apt-get install -y gcc g++ make cmake zlib1g zlib1g-dev openssl libsqlite3-dev libssl-dev libffi-dev libbz2-dev libxslt1-dev unzip pciutils net-tools libblas-dev gfortran libblas3
   pip3 install attrs numpy decorator sympy cffi pyyaml pathlib2 psutil protobuf scipy requests absl-py --user
   ```

   在当前用户下执行以下命令安装cann软件包

   ```linux
   #获取cann软件包
   wget xxx.run
   #增加对软件包的可执行权限
   chmod +x 软件包名.run
   #执行如下命令校验软件包安装文件的一致性和完整性
   ./软件包名.run --check
   #执行如下命令安装软件
   ./软件包名.run --install
   #安装完成后，若显示如下信息，则说明软件安装成功：xxx install success
   #将如下命令写入当前用户目录下的.bashrc文件中
   source /usr/local/Ascend/ascend-toolkit/set_env.sh
   export LD_LIBRARY_PATH=/usr/local/Ascend/ascend-toolkit/latest/aarch64-linux/devlib/:$LD_LIBRARY_PATH
   #执行如下命令生效环境变量
   source ~/.bashrc
   ```

#### 样例说明

1.  [Yolov3_Raspi_Camera](https://gitee.com/HiCAID/rpi-ascend-app/tree/master/app/Yolov3_Raspi_Camera/python)：基于Caffe的yolov3目标检测模型，转换成昇腾AI处理器能识别的om模型，使用树莓派摄像头进行实时检测推理
3.  [Yolov3_Usb_Camera](https://gitee.com/HiCAID/rpi-ascend-app/tree/master/app/Yolov3_Usb_Camera/python)：基于Caffe的yolov3目标检测模型，转换成昇腾AI处理器能识别的om模型，使用USB摄像头进行实时检测推理
4.  [Yolov3_Video](https://gitee.com/HiCAID/rpi-ascend-app/tree/master/app/Yolov3_Video/python)：基于Caffe的yolov3目标检测模型，转换成昇腾AI处理器能识别的om模型，使用mp4视频进行实时检测推理
