#!/bin/bash
ScriptPath="$( cd "$(dirname "$BASH_SOURCE")" ; pwd -P )"
Src=${ScriptPath}/../src


function main()
{
    cd ${Src}
    echo "[INFO] The sample starts to run"
    running_command="python3 Yolov3_Video.py"
    for i in {0..3}
    do
        ${running_command} &
    done
    wait
    if [ $? -ne 0 ];then
        return 1
    fi
}
main
